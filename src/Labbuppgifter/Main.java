package Labbuppgifter;

import java.util.ArrayList;
import java.util.List;

class PrimtalChecker implements Runnable {
    int start;
    int end;
    boolean primtal;



    public PrimtalChecker(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public void run() {
        List<Integer> list = new ArrayList<>();
        for(int i = start; i <= end; i++) {
            primtal = true;
            if (i < 2) {
                primtal = false;
            }
            for (int x = 2; x < i; x++) {
                if (i % x == 0) {
                    primtal = false;
                }
            } if (primtal) {
                list.add(i);
            }
        }
        System.out.println(list);
    }
}

public class Main {

    public static void main(String[] args) throws InterruptedException {

        Thread thread1 = new Thread(new PrimtalChecker(0, 350000));
        Thread thread2 = new Thread(new PrimtalChecker(350001, 500000));

        thread1.start();
        thread1.join();
        thread2.start();
        thread2.join();

    }
}
